package algo.listinterface;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class SinglyLinkedList<E> extends AbstractList<E> {
  int count;
  Node<E> head;

  // use the tail to perform add(E) in O(1)
  Node<E> tail;

  /**
   * Construct an empty list.
   *
   * @post generates an empty list
   */
  SinglyLinkedList() {
    head = null;
    tail = null;
    count = 0;
  }

  /**
   * Add an object to tail of list. O(1)
   */
  @Override
  public boolean add(E value) {
    Node<E> node = new Node<>(value);
    if (head == null) {
      head = tail = node;
    } else {
      tail.nextElement = node;
      tail = node;
    }
    count++;
    return true;
  }

  /**
   * Add at first and last will be O(1). Other index will run at O(n).
   */
  @Override public void add(int index, E element) {
    if (index < 0 || index > count) {
      throw new IndexOutOfBoundsException();
    }

    count++;
    Node<E> newNode = new Node<>(element);

    if (index == 0) { // add at first
      newNode.nextElement = head;
      head = newNode;
      return;
    }

    if (index == count) {
      add(element);
      return;
    }

    Node<E> node = head;
    while (index > 1) {
      index--;
      node = node.nextElement;
    }

    newNode.nextElement = node.nextElement;
    node.nextElement = newNode;
  }

  /**
   * Add all items in the collection. O(n) in general. O(1) if the input is of type {@link
   * SinglyLinkedList}, so that we can access internal structure, and perform the append faster.
   */
  @Override public boolean addAll(Collection<? extends E> c) {
    if (c instanceof SinglyLinkedList) {
      @SuppressWarnings("unchecked") SinglyLinkedList<E> list = (SinglyLinkedList) c;
      if (tail == null) {
        head = list.head;
        tail = list.tail;
        count = list.count;
      } else {
        tail.nextElement = list.head;
        tail = list.tail;
      }
      return true;
    }

    return super.addAll(c);
  }

  /**
   * O(n) in general. O(i) with i = index in case the input is of type SinglyLinkedList.
   */
  @Override public boolean addAll(int index, Collection<? extends E> c) {
    if (index < 0 || index > count) {
      throw new IndexOutOfBoundsException();
    }

    if (c.size() == 0) {
      return true;
    }

    if (c instanceof SinglyLinkedList) {
      Node<E> node = head;
      while (index > 0) {
        index--;
        node = node.nextElement;
      }

      @SuppressWarnings("unchecked") SinglyLinkedList<E> list = (SinglyLinkedList) c;
      list.tail.nextElement = node.nextElement;
      node.nextElement = list.head;
      return true;
    }
    return super.addAll(index, c);
  }

  /**
   * Check to see if a value is in list.
   *
   * @param value The value sought.
   * @return True if value is within list.
   */
  @Override
  public boolean contains(Object value) {
    Node<E> finger = head;
    while (finger != null && !finger.value().equals(value)) {
      finger = finger.nextElement;
    }
    return finger != null;
  }

  @Override public boolean containsAll(Collection<?> c) {
    if (c instanceof SinglyLinkedList) {
      Node theirNode = ((SinglyLinkedList) c).head;
      Node myNode = head;
      while (myNode != null && theirNode != null && myNode.value() == theirNode.value()) {
        myNode = myNode.nextElement;
        theirNode = theirNode.nextElement;
      }

      return theirNode == null;
    }
    return super.containsAll(c);
  }

  @Override public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    if (o instanceof SinglyLinkedList) {
      return ((SinglyLinkedList) o).head == head;
    }

    return super.equals(o);
  }

  @Override public void forEach(Consumer<? super E> action) {
    Node<E> node = this.head;
    while (node != null) {
      action.accept(node.value());
      node = node.nextElement;
    }
  }

  /**
   * Remove a value from list.  At most one value will be removed.
   *
   * @param value The value to be removed.
   * @return The actual value removed.
   */
  @Override
  public boolean remove(Object value) {
    Node<E> finger = head;
    Node<E> previous = null;
    while (finger != null &&
        !finger.value().equals(value)) {
      previous = finger;
      finger = finger.next();
    }
    // finger points to target value
    if (finger != null) {
      // we found element to remove
      if (previous == null) // it is first
      {
        head = finger.next();
      } else {              // it's not first
        previous.setNext(finger.next());
      }
      count--;
      return true;
    }
    // didn't find it, return null
    return false;
  }

  @Override public E remove(int index) {
    if (index < 0 || index >= count) {
      throw new IndexOutOfBoundsException();
    }

    count--;
    if (index == 0) {
      E result = head.value();
      head = head.nextElement;
      return result;
    }

    Node<E> node = this.head;
    while (index > 1) {
      node = node.nextElement;
    }

    E result = node.nextElement.value();
    node.nextElement = node.nextElement.nextElement;
    return result;
  }

  @Override public boolean removeIf(Predicate<? super E> filter) {
    boolean modified = false;
    while (head != null && filter.test(head.value())) {
      count--;
      head = head.nextElement;
      modified = true;
    }

    if (head == null) {
      return modified;
    }

    Node<E> node = head;

    while (node != null && node.nextElement != null) {
      if (filter.test(node.nextElement.value())) {
        count--;
        node.nextElement = node.nextElement.nextElement;
        modified = true;
      } else {
        node = node.nextElement;
      }
    }
    return modified;
  }

  /**
   * Determine number of elements in list.
   *
   * @return The number of elements in list.
   */
  @Override
  public int size() {
    return count;
  }

  /**
   * Remove all values from list.
   */
  @Override
  public void clear() {
    head = null;
    tail = null;
    count = 0;
  }

  /**
   * Get value at location i.
   *
   * @param i position of value to be retrieved.
   * @return value retrieved from location i (returns null if i invalid)
   */
  @Override
  public E get(int i) {
    checkIndex(i);
    Node<E> finger = head;
    // search for ith element or end of list
    while (i > 0) {
      finger = finger.next();
      i--;
    }
    return finger.value();
  }

  private void checkIndex(int i) {
    if (i >= size() || i < 0) throw new IndexOutOfBoundsException();
  }

  /**
   * Set value stored at location i to object o, returning old value.
   *
   * @param i location of entry to be changed.
   * @param o new value
   * @return former value of ith entry of list.
   */
  @Override
  public E set(int i, E o) {
    checkIndex(i);

    Node<E> finger = head;
    // search for ith element or end of list
    while (i > 0) {
      finger = finger.next();
      i--;
    }
    // get old value, update new value
    E result = finger.value();
    finger.setValue(o);
    return result;
  }

  @Override public void sort(Comparator<? super E> c) {
    if (count < 2) return;
    Node<E> newHead = new Node<>(head.value());
    Node<E> node = head.nextElement;
    while (node != null) {
      Node<E> copy = new Node<>(node.value());
      if (c.compare(node.value(), newHead.value()) <= 0) {
        copy.nextElement = newHead;
        newHead = copy;
        node = node.nextElement;
        continue;
      }

      Node<E> n = newHead;
      while (n.nextElement != null && c.compare(n.nextElement.value(), node.value()) <= 0) {
        n = n.nextElement;
      }
      copy.nextElement = n.nextElement;
      n.nextElement = copy;

      node = node.nextElement;
    }
    head = newHead;
  }

  /**
   * Determine first location of a value in list.
   *
   * @param value value sought
   * @return index (0 is first element) of value, or -1
   */
  @Override
  public int indexOf(Object value) {
    int i = 0;
    Node<E> finger = head;
    // search for value or end of list, counting along way
    while (finger != null && !finger.value().equals(value)) {
      finger = finger.next();
      i++;
    }
    // finger points to value, i is index
    if (finger == null) {   // value not found, return indicator
      return -1;
    } else {
      // value found, return index
      return i;
    }
  }

  @Override public boolean isEmpty() {
    return count == 0;
  }

  /**
   * Determine last location of a value in list.
   *
   * @param value value sought.
   * @return index (0 is first element) of value, or -1
   * @pre value is not null
   * @post returns the (0-origin) index of value, or -1 if value is not found
   */
  @Override
  public int lastIndexOf(Object value) {
    int result = -1;        // assume not found, return -1
    int i = 0;
    Node<E> finger = head;
    // search for last matching value, result is desired index
    while (finger != null) {
      // a match? keep track of location
      if (finger.value().equals(value)) result = i;
      finger = finger.next();
      i++;
    }
    // return last match
    return result;
  }

  /**
   * Returns an iterator traversing list from head to tail.
   *
   * @return An iterator to traverse list.
   * @post returns enumeration allowing traversal of list
   */
  @Override
  public Iterator<E> iterator() {
    return new OwnListIterator<>(this);
  }

  /**
   * Construct a string representing list.
   *
   * @return A string representing list.
   * @post returns a string representing list
   */
  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    s.append("<SinglyLinkedList:");
    Enumeration li = (Enumeration) iterator();
    while (li.hasMoreElements()) {
      s.append(" ").append(li.nextElement());
    }
    s.append(">");
    return s.toString();
  }

  /**
   * Explicit public the removeRange method from {@link AbstractList}, since it is not a part of
   * public interfaces.
   */
  public void removeRange(int fromIndex, int toIndex) {
    if (fromIndex < 0 || fromIndex >= count || toIndex < 0 || toIndex > count) {
      throw new IndexOutOfBoundsException();
    }
    count -= (toIndex - fromIndex);
    Node<E> left = head;
    while (fromIndex > 1) {
      fromIndex--;
      toIndex--;
      left = left.nextElement;
    }

    Node<E> right = left.nextElement;

    while (toIndex > 1) {
      toIndex--;
      right = right.nextElement;
    }

    left.nextElement = right;
  }

  @Override public void replaceAll(UnaryOperator<E> operator) {
    Node<E> node = this.head;
    while (node != null) {
      node.setValue(operator.apply(node.value()));
      node = node.nextElement;
    }
  }

  /*
   * The iterator class is moved to an inner class of SinglyLinkedList so that it can access private
   * fields of the parent class. With that, method like remove() can make change to the internal
   * counter of the parent class.
   */
  private static class OwnListIterator<E> extends AbstractIterator<E> {
    private final SinglyLinkedList<E> list;
    /**
     * The reference to currently considered element within list.
     */
    Node<E> current;

    /**
     * Previous node, use for removal of the current node.
     */
    Node<E> previous;

    Node<E> previousOfPrevious;

    /**
     * The head of list.
     */
    Node<E> head;

    /**
     * Construct an iterator that traverses list beginning at t.
     *
     * @post returns an iterator that traverses a linked list
     */
    public OwnListIterator(SinglyLinkedList<E> list) {
      this.list = list;
      this.head = list.head;
      reset();
    }

    /**
     * Reset iterator to beginning of the structure.
     *
     * @post iterator is reset to beginning of traversal
     */
    @Override
    public void reset() {
      current = head;
      previous = null;
      previousOfPrevious = null;
    }

    /**
     * Determine if the iteration is finished.
     *
     * @return True if the iterator has more elements to be considered.
     * @post returns true if there is more structure to be viewed: i.e., if value (next) can return
     * a useful value.
     */
    @Override
    public boolean hasNext() {
      return current != null;
    }

    /**
     * Return current value and increment Iterator.
     *
     * @return The current value, before increment.
     * @pre traversal has more elements
     * @post returns current value and increments iterator
     */
    @Override
    public E next() {
      if (current == null) {
        throw new NoSuchElementException();
      }

      E temp = current.value();

      if (previousOfPrevious == null || previousOfPrevious.nextElement == previous) {
        previousOfPrevious = previous;
      }
      previous = current;

      current = current.next();
      return temp;
    }

    /**
     * Return structure's current object reference.
     *
     * @return E currently referenced.
     * @pre traversal has more elements
     * @post returns current value referenced by iterator
     */
    public E get() {
      if (current == null) {
        throw new NoSuchElementException();
      }
      return current.value();
    }

    @Override
    public void remove() {
      if (current == head) {
        throw new IllegalStateException("next() is not called yet");
      }

      if (previousOfPrevious != null && previous != previousOfPrevious.nextElement) {
        throw new IllegalStateException("item is already removed");
      }

      list.count--;

      if (previousOfPrevious != null) {
        previousOfPrevious.nextElement = current;
      } else {
        head.nextElement = current.nextElement;
        head.setValue(current.value());
        reset();
      }
    }
  }

  @Override public Object[] toArray() {
    Object[] result = new Object[count];
    Node<E> node = head;
    for (int i = 0; i < count; i++) {
      result[i] = node.value();
      node = node.nextElement;
    }
    return result;
  }
}
