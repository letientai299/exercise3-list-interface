package algo.listinterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SinglyLinkedListTest {
  private SinglyLinkedList<String> list;

  @Before
  public void setup() {
    list = new SinglyLinkedList<>();
  }

  @Test
  public void add_ok() {
    for (int i = 0; i < 100; i++) {
      boolean result = list.add("hello");
      // This is not a Set, so add should always be ok, unless some very edge case, e.g. out of memory,
      // but that is hard to test.
      assertTrue(result);
    }
    assertEquals(list.count, 100);

    Node<String> node = list.head;
    for (int i = 0; i < list.count; i++) {
      assertEquals(node.value(), "hello");
      node = node.nextElement;
    }

    assertNull(node);
  }

  @Test
  public void addAll_emptyInput() {
    list.addAll(Collections.emptyList());
    assertEquals(list.size(), 0);
  }

  @Test
  public void addAll_inputHasValues() {
    int n = 5;
    list.addAll(Collections.nCopies(n, "hello"));
    for (int i = 0; i < n; i++) {
      String v = list.get(i);
      assertEquals("hello", v);
    }

    assertEquals(list.size(), n);
  }

  @Test
  public void addAll_listHasValue_inputHasValue() {
    int n = 4;
    int m = 5;
    for (int i = 0; i < n; i++) {
      list.add("hello");
    }

    list.addAll(Collections.nCopies(m, "world"));
    assertEquals(list.size(), m + n);

    Node node = list.head;
    for (int i = 0; i < n; i++) {
      assertEquals(node.value(), "hello");
      node = node.nextElement;
    }

    for (int i = 0; i < m; i++) {
      assertEquals(node.value(), "world");
      node = node.nextElement;
    }
    assertNull(node);
  }

  @Test
  public void addAll_index_emptyInput() {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }
    list.addAll(Collections.emptyList());
    assertEquals(list.size(), 10);
  }

  @Test
  public void addAll_index_inputHasValues() {
    int n = 7;
    int m = 5;
    int h = 3;
    for (int i = 0; i < n; i++) {
      list.add("hello");
    }

    list.addAll(h, Collections.nCopies(m, "world"));
    assertEquals(list.size(), m + n);

    Node node = list.head;
    for (int i = 0; i < h; i++) {
      assertEquals(node.value(), "hello");
      node = node.nextElement;
    }

    for (int i = 0; i < m; i++) {
      assertEquals(node.value(), "world");
      node = node.nextElement;
    }

    for (int i = 0; i < n - h; i++) {
      assertEquals(node.value(), "hello");
      node = node.nextElement;
    }
    assertNull(node);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void add_index_outOfRange() {
    list.add(1, "hello");
  }

  @Test
  public void add_index_inRange() {
    list.add(0, "hello0");
    list.add(1, "hello1");
    list.add(1, "hello2");

    String result = list.get(1);
    assertEquals("hello2", result);
  }

  @Test
  public void clear_noThing() {
    list.clear();
  }

  @Test
  public void clear_hasValues() {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    assertEquals(list.size(), 10);
    list.clear();
    assertEquals(list.size(), 0);
  }

  @Test
  public void size_empty() {
    assertEquals(list.size(), 0);
    assertEquals(list.count, 0);
  }

  @Test
  public void size_hasValues() {
    int n = 10;
    for (int i = 0; i < n; i++) {
      list.add(String.valueOf(i));
    }

    assertEquals(list.size(), n);
    assertEquals(list.count, n);
  }

  @Test
  public void indexOf_found() {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    for (int i = 0; i < 10; i++) {
      int shouldBeI = list.indexOf(String.valueOf(i));
      assertEquals(i, shouldBeI);
    }
  }

  @Test
  public void indexOf_notFound() {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    int notFoundIndex = list.indexOf("11");
    assertEquals(-1, notFoundIndex);
  }

  @Test
  public void lastIndexOf_found() {
    int n = 5;
    for (int i = 0; i < n; i++) {
      list.add("hi");
    }

    int last = list.lastIndexOf("hi");
    assertEquals(n - 1, last);
  }

  @Test
  public void lastIndexOf_notFound() {
    int n = 5;
    for (int i = 0; i < n; i++) {
      list.add("hi");
    }

    int last = list.lastIndexOf("hello");
    assertEquals(-1, last);
  }

  @Test
  public void get_validIndex() {
    list.add("something");
    String s = list.get(0);
    assertEquals("something", s);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void get_invalidIndex_tooHigh() {
    list.get(1);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void get_invalidIndex_tooLow() {
    list.get(-1);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void remove_invalidIndex() {
    list.remove(0);
  }

  @Test
  public void remove_validIndex() {
    list.add("hi");
    String removed = list.remove(0);
    assertEquals("hi", removed);
  }

  @Test
  public void removeRange() throws Exception {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    // removeRange is not in public API of List interface, hence we have to cast it to our concrete type
    SinglyLinkedList<String> concreteTypeList = (SinglyLinkedList<String>) list;
    concreteTypeList.removeRange(3, 9);

    // Check the size of the list after removal
    assertEquals(list.size(), 4);
    System.out.println(list);

    // elements in the list after removeRange.
    List<String> expectedValues = Arrays.asList("0", "1", "2", "9");

    for (int i = 0; i < expectedValues.size(); i++) {
      assertEquals(expectedValues.get(i), list.get(i));
    }
  }

  @Test
  public void replaceAll() throws Exception {
    int n = 10;
    for (int i = 0; i < n; i++) {
      list.add(String.valueOf(i));
    }

    // We will replace all the strings that were even numbers to the string "even"
    list.replaceAll(s -> 0 == Integer.valueOf(s) % 2 ? "even" : s);

    // now check that the result are what we expected.
    for (int i = 0; i < n; i++) {
      String s = list.get(i);
      // If the index are even, the the value must be replaced by "even"
      if (i % 2 == 0) {
        assertEquals("even", s);
      } else {
        // otherwise, it stays as number.
        assertEquals(String.valueOf(i), s);
      }
    }
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void set_invalidIndex_tooLow() {
    list.set(-1, "whatever");
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void set_invalidIndex_tooHigh() {
    list.set(1, "whatever");
  }

  @Test
  public void set_null() {
    list.add("hi");
    String first = list.get(0);
    assertEquals("hi", first);

    list.set(0, null);

    first = list.get(0);
    assertEquals(null, first);
  }

  @Test
  public void set_notNull() {
    list.add("hi");
    String first = list.get(0);
    assertEquals("hi", first);

    list.set(0, "hello");

    first = list.get(0);
    assertEquals("hello", first);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void subList_invalidRange() {
    list.subList(0, 1);
  }

  @Test
  public void subList_empty() {
    List<String> strings = list.subList(0, 0);
    assertEquals(0, strings.size());
  }

  @Test
  public void subList_validRangeHasValues() {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    int start = 3;
    int end = 7;
    List<String> strings = list.subList(start, end);
    System.out.println(strings);
    for (int i = start; i < end; i++) {
      assertEquals(String.valueOf(i), strings.get(i - start));
    }
  }

  @Test
  public void iterator() {
    for (int i = 0; i < 20; i++) {
      list.add(String.valueOf(i));
    }

    list.removeIf(next -> Integer.valueOf(next) > 5);
    System.out.println(list);
  }

  @Test
  public void removeIf() {
    for (int i = 1; i <= 11; i++) {
      list.add(String.valueOf(i));
    }
    // Remove any item that doesn't start with "1"
    boolean hasAnythingRemoved = list.removeIf(item -> !item.startsWith("1"));
    assertTrue(hasAnythingRemoved);
    System.out.println(list);
    list.forEach(item -> assertTrue(item.startsWith("1")));
  }

  // Ignore the warning by Intellij since the call to List::add() is intended
  @SuppressWarnings("UseBulkOperation") @Test
  public void forEach() throws Exception {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    List<String> collection = new ArrayList<>();

    // check that forEach works by performing an accumulation
    list.forEach(collection::add);

    assertEquals(list.size(), collection.size());
  }

  @Test
  public void containsAll() throws Exception {
    List<String> anotherList = new SinglyLinkedList<>();

    for (int i = 0; i < 10; i++) {
      String e = String.valueOf(i);
      list.add(e);
      anotherList.add(e);
    }

    assertTrue(list.containsAll(anotherList));
    assertTrue(anotherList.containsAll(list));
  }

  @Test
  public void containsAll_empty() throws Exception {
    List<String> anotherList = new SinglyLinkedList<>();

    for (int i = 0; i < 10; i++) {
      String e = String.valueOf(i);
      list.add(e);
      anotherList.add(e);
    }

    anotherList.add("Hello");

    assertTrue(anotherList.containsAll(list));
    assertFalse(list.containsAll(anotherList));
  }

  @Test
  public void retainAll() throws Exception {
    List<String> evenList = new ArrayList<>();

    int n = 10;
    for (int i = 0; i < n; i++) {
      list.add(String.valueOf(i));

      // The evenList contains only even "number"s
      if (i % 2 == 0) {
        evenList.add(String.valueOf(i));
      }
    }

    list.retainAll(evenList);
    assertEquals(list.size(), n / 2);

    list.forEach(s -> assertTrue(Integer.valueOf(s) % 2 == 0));
  }

  @Test
  public void retainAll_union() throws Exception {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    List<String> anotherList = new ArrayList<>();
    for (int i = 5; i < 15; i++) {
      anotherList.add(String.valueOf(i));
    }

    list.retainAll(anotherList);

    // After retainAll, the list should only contains number from 5 to 9
    assertEquals(list.size(), 5);
    for (int i = 5; i < 10; i++) {
      assertEquals(list.get(i - 5), String.valueOf(i));
    }
  }

  @Test
  public void stream() throws Exception {
    for (int i = 0; i < 10; i++) {
      list.add(String.valueOf(i));
    }

    List<String> collected = list.stream().map(s -> "Number: " + s).collect(Collectors.toList());
    for (int i = 0; i < 10; i++) {
      assertEquals("Number: " + i, collected.get(i));
    }
  }

  @Test
  public void spliterator() throws Exception {
    int n = 10;
    for (int i = 0; i < n; i++) {
      list.add(String.valueOf(i));
    }

    Spliterator<String> listSpliterator = list.spliterator();
    Spliterator<String> newPartition = listSpliterator.trySplit();

    assertEquals(n, listSpliterator.estimateSize() + newPartition.estimateSize());
  }

  @Test
  public void toArray() throws Exception {
    int n = 10;
    for (int i = 0; i < n; i++) {
      list.add(String.valueOf(i));
    }

    String[] strings = list.toArray(new String[] {});

    for (int i = 0; i < n; i++) {
      assertEquals(String.valueOf(i), strings[i]);
    }
  }

  @Test
  public void sort_empty() {
    // Just run and expect that the there's no exception
    list.sort(String::compareTo);
  }

  @Test
  public void sort_singleValue() throws Exception {
    list.add("hello");
    list.sort(String::compareTo);

    System.out.println(list);
    assertEquals(list.size(), 1);
    assertEquals(list.get(0), "hello");
  }

  @Test
  public void sort_multipleValues() throws Exception {
    // To make this test easier, we use another list
    List<Integer> list = new SinglyLinkedList<>();
    for (int i = 0; i < 10; i++) {
      list.add(i);
    }

    Comparator<Integer> revertOrder = (a, b) -> {
      if (a.equals(b)) return 0;

      if (a > b) return -1;

      return 1;
    };

    list.sort(revertOrder);
    System.out.println(list);
    for (int i = 0; i < 9; i++) {
      assertTrue(list.get(i) > list.get(i + 1));
    }
  }
}

