Exercise 3 - List interface
===========================

- `SinglyLinkedList` is copied an modified from the `source.tar.gz` on the web
site provided from docx. Now the class inherit from java `AbstractList` instead
of Mr. Baily's version. All missing methods are implemented.

- `OwnListIterator` is copied and modified from `SinglyLinkedListIterator`. We
  have move it into the `SinglyLinkedList` to make private fields accessible
  from the iterator, particular the `count` field, which should be updated from
  `OwnListIterator::remove()` method.

- A large set of method (`sort`, `replaceIf`, `forEach`, `stream`, ...) are
  already provided by java `AbstractList`, hence, we didn't roll our own
  version. But, we have added test cases to ensure that they did work as
  expected. Those test cases also help to detect bugs in
  `SinglyLinkedListIterator::remove()` and `next()` methods, which are heavily
  used by `retainAll`, `removeIf`, `replaceIf` and `removeRange`.
